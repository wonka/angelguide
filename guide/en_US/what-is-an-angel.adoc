== icon:hands-helping[size=fw]  What is an angel?

NOTE: Everyone who invests their free time in our events is an angel. +

While the most obvious angels to most participants are the helpers staffing the cash desk, checking wrist bands at the doors or selling drinks at the bars, there are tons of jobs to be done "behind the scenes" that are barely noticeable (or only become apparent if something goes wrong).

All these people, from a new person, who sits at a smoker door for one shift at their first congress up to the experienced people in the organization team who do the high level planning in the months and weeks before the event - they all are angels.

=== Perks

Being an angel also comes with some perks. Though we hope participating is enough of a reward, here is a list of things exclusive to angels.

* Hanging out at heaven, Angel Hackcenter, using its wardrobe and chill out area.
* Free coffee and (sparkling) water.
* Free food for night shifts.

=== Rewards

Having contributed a certain amount of time will also grant you access to:

* Awesome warm vegan and vegetarian meals.
* Sleeping arrangements at nearby gyms.
* The famous limited™ Angel T-Shirt in congress design.

[IMPORTANT]
.Resources Limitations
====
Please keep in mind that our resources are limited. Not everyone might get a T-Shirt, and meals and sleeping arrangements are also finite resources.
====

=== Expectations

IMPORTANT: Helping at our events also comes with some simple but important expectations of you. +

* Be on time to your shift or give heaven notice early.
* Be well-rested, sober and fed.
* Be open-minded and friendly spirited.
* Live our moral values:
** Be friendly to each other.
** All creatures are welcome.

=== Child Angels

NOTE: Most angel jobs on our events are designed with adults in mind. +

While we do not ask for your passport to check your legal age, we will probably not hand you an angel badge if you are clearly underage. In general, children should never be left unattended and therefore are not allowed to sign up for any angel shifts themselves when not accompanied by a parent or legal guardian.

However, every day is "bring your kid to work day", so if your kids are old enough to attend our events, you can bring them to most of the angel shifts as well.

Please come by heaven and talk to our shift coordinators and we will arrange everything as needed. Of course, if your kids want to get an angel badge as well, they can get one and they will also be eligible for a T-shirt if they work enough shifts.

NOTE: Sadly, we are currently not able to provide children's size T-shirts. +

Nonetheless, please come to heaven and let us add your kid to any shifts as "n+1" instead of simply singing them up since the required number of angels needed for a shift is calculated on the basis of adults.

Please note that there are a few exceptions to this rule:

* Children **may not work bar shifts** due to the handling of alcoholic beverages.
* Children will be **denied access to the LOC** due to the heavy machinery and other work-related dangers in that area, so even if the shift itself may be appropriate, you will probably not be able to get there.

In addition, if you want to bring your kids to a shift that needs a special angel type introduction, please ask the appropriate angel type supporter(s) beforehand if this is okay.
