=== CERT [small]#(Chaos Emergency Response Team)#

If you have a physical emergency of any kind (medical / fire) call 112 (with your DECT!)

They take care of immediate or longer term medical problems and handle emergencies like fires.
