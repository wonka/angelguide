Sehr geehrter Leser, +

Danke, dass du Teil der Chaos-Community bist und dir die Zeit nimmst, dich zu informieren, wie du am besten helfen kannst! +

Dieser Leitfaden versucht, alle notwendigen Informationen und Eigenheiten zu vermitteln, die dir bei der Mithilfe bei einem Chaosevent begegnen können. +

Wie bei jedem anderen Leitfaden ist es fast unmöglich, einen vollständigen Leitfaden zu schreiben, also sieh über unsere eigene Unwissenheit hinweg und fühl dich frei, die mailto:angel-guide@c3heaven.de[Autoren] mit jedem neuen Wissen zu erleuchten, auf das du stößt.
