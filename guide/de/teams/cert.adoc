=== CERT [small]#(Chaos Emergency Response Team)#

Wenn du einen physischen Notfall jeglicher Art (medizinisch / Feuer) hast, rufe 112 (mit Deinem DECT!) an.

Die Leute vom CERT kümmern sich um sofortige oder längerfristige medizinische Probleme und behandeln Notfälle wie Brände.
