=== VOC [small]#(Video Operation Center)#

Das VOC besitzt alle glücklichen Katzen, die auf den Bildschirmen winken.

Das Streaming von zu Hause aus und die Behandlung deiner Post-Kongress-Depression mit interessanten Vorträgen wird durch das VOC ermöglicht.
Die Video- und Audioaufzeichnung und -mischung sowie das Rendern und Hochladen der Videos werden von ihnen übernommen.
