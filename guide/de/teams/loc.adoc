=== LOC [small]#(Logistics Operation Center)#

Das LOC bringt vor der Veranstaltung alles an den Veranstaltungsort und schafft es, es auch nach der Abschlussveranstaltung aus dem Veranstaltungsort wieder abzutransportieren.

Sie verwalten das Lager und behalten den Überblick über alle benötigten schweren Maschinen.

Geh nur dorthin, wenn du nüchtern und dich deiner Umgebung bewusst bist, da du nicht von einem Gabelstabler überfahren werden willst.
